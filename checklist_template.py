#!/usr/bin/env python3
# 
# This program is part of FlightSim Checklist Tools
#
# Copyright 2023 Randolf Lemmont <https://gitlab.com/randolf.lemmont>
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import json
from pathlib import Path
import argparse
from jinja2 import Environment, FileSystemLoader, select_autoescape

parser = argparse.ArgumentParser()
parser.add_argument("-n", metavar='TITLE', default='Checklists', type=str,
                    help="Title for the converted checlists")
parser.add_argument("-t", metavar='TEMPLATE', type=str, required=True,
                    help="Jinja2 teplate file")
parser.add_argument("infile", metavar="JSON_FILE", type=str,
                    help="Checklist in JSON format")
args = parser.parse_args()

with open(args.infile, "r") as f:
    data = json.load(f)

templatefile = Path(args.t)
templatedir = templatefile.parent
env = Environment(
    loader=FileSystemLoader(templatedir),
    autoescape=select_autoescape()
)
template = env.get_template(templatefile.name)

print(template.render(checklistgroups=data, title=args.n))
