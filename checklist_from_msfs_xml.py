#!/usr/bin/env python3
# 
# This program is part of FlightSim Checklist Tools
#
# Copyright 2023 Randolf Lemmont <https://gitlab.com/randolf.lemmont>
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import sys
import json
import argparse
import xml.etree.ElementTree as ET

parser = argparse.ArgumentParser()
parser.add_argument("-g", metavar='GROUP:PREFIX', type=str, nargs='+', required=True,
                    help="Start a new group at list starting with PREFIX. "
                    "Can be repeated multiple times, at least one group is required. "
                    "First group should be without PREFIX.")
args = parser.parse_args()

tree = ET.parse(sys.stdin)
xroot = tree.getroot()
xchecklist = xroot.find('Checklist.Checklist')

groupnames = iter(args.g)

# initialize output data structure with first group of checklists
checklists = []
data = [{"name": next(groupnames), "checklists": checklists}]
try:
    nextgroup = next(groupnames).split(':')
except StopIteration:
    nextgroup = None

for xstep in xchecklist.findall('Step'):
    for xpage in xstep.findall('Page'):
        items = []
        for xcheckpoint in xpage:
            if xcheckpoint.tag != "Checkpoint":
                print('Ignoring unknown tag:', xcheckpoint.tag, file=sys.stderr)
                continue
            xchkdesc = xcheckpoint.find("CheckpointDesc")
            items.append({
                "type": "CHALLENGE",
                "text": xchkdesc.attrib['SubjectTT'],
                "comment": "",
                "response": xchkdesc.attrib['ExpectationTT'],
                })
        checklistname = xpage.attrib['SubjectTT']
        if nextgroup is not None:
            if checklistname.startswith(nextgroup[1]):
                # start a next group of checklists
                checklists = []
                data.append({"name": nextgroup[0], "checklists": checklists})
                try:
                    nextgroup = next(groupnames).split(':')
                except StopIteration:
                    nextgroup = None
        checklists.append({"name": checklistname, "items": items})
print(json.dumps(data, indent=2))
